<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer/{id?}', function ($id = 'No customer was provided') {
   if($id =='No customer was provided')
    return $id ;
    return 'welcom' . $id;
})->name('customers');